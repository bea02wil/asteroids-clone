using System;
using UnityEngine;
using UnityEngine.UI;
using DmitryKonstantinov.AsteroidsClone.Utils;
using DmitryKonstantinov.AsteroidsClone.Unit;
using DmitryKonstantinov.AsteroidsClone.Unit.Weapon;

namespace DmitryKonstantinov.AsteroidsClone.Ui
{
    public class UiHandler : SingletonHandler<UiHandler>
    {
        [Header("Object references")]

        [SerializeField] private ScoreCounter scoreCounter;
        [SerializeField] private Player player;
        [SerializeField] private Laser laser;
        [SerializeField] private GameObject gameoverPanel;

        [Header("UI Texts")]

        [SerializeField] private Text coordinates;
        [SerializeField] private Text rotationAngle;
        [SerializeField] private Text speed;
        [SerializeField] private Text laserCharges;
        [SerializeField] private Text laserCooldown;
        [SerializeField] private Text points;
        [SerializeField] private Text gameoverPoints;
        [SerializeField] private Text weapon;

        private UiHandlerLogic uiHandlerLogic;
        
        protected override void Awake()
        {
            base.Awake();

            uiHandlerLogic = new UiHandlerLogic(player);
            uiHandlerLogic.HideHudElement(gameoverPanel);
        }

        private void Update()
        {
            uiHandlerLogic.ShowHudMessage(
                coordinates,
                $"Coordinates: X:{Math.Round(player.transform.position.x, 2)} Y:{Math.Round(player.transform.position.y, 2)}"
               );

            uiHandlerLogic.ShowHudMessage(points, $"Points: {scoreCounter.playerScore}");
            uiHandlerLogic.ShowHudMessage(gameoverPoints, $"Points: {scoreCounter.playerScore}");
            uiHandlerLogic.ShowHudMessage(weapon, $"Weapon: {player.UnitData.unitWeapon.name}");
            
            uiHandlerLogic.ShowHudMessage(speed, $"Speed: {Math.Round(player.UnitData.moveSpeed, 2)}");
            uiHandlerLogic.ShowHudMessage(rotationAngle, $"Rotation angle: {(int)player.transform.localEulerAngles.z}");

            uiHandlerLogic.ShowHudMessage(laserCharges, $"Laser charges: {laser.ammo}");
            uiHandlerLogic.ShowHudMessage(laserCooldown, null, laser);

            uiHandlerLogic.DisplayGameoverPanel(gameoverPanel);
        }
    }
}

