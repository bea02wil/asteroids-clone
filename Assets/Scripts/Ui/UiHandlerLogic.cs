using DmitryKonstantinov.AsteroidsClone.Unit;
using DmitryKonstantinov.AsteroidsClone.Unit.Weapon;
using UnityEngine;
using UnityEngine.UI;

namespace DmitryKonstantinov.AsteroidsClone.Utils
{
    public class UiHandlerLogic
    {
        private Player player;

        public UiHandlerLogic(Player player)
        {
            this.player = player;
        }

        public void ShowHudMessage(Text hud, string hudMessage, Object gameObject = null)
        {
            if (gameObject is Laser)
            {
                var laser = gameObject as Laser;
                if (laser.ammo < laser.DefaultAmmoCount)
                {
                    hud.text = $"Laser cooldown: {(int)laser.restoreAmmoTime}";
                }
                else
                {
                    hud.text = "Laser cooldown:";
                }
            }
            else
            {
                hud.text = hudMessage;
            }     
        }

        public void DisplayGameoverPanel(GameObject panel)
        {
            if (player.IsDead)
            {
                DisplayHudElement(panel);
            }
        }

        public void DisplayHudElement(GameObject element)
        {
            element.SetActive(true);
        }

        public void HideHudElement(GameObject element)
        {
            element.SetActive(false);
        }
    }


}

