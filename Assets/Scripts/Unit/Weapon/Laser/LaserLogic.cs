using DmitryKonstantinov.AsteroidsClone.Utils;
using UnityEngine;

namespace DmitryKonstantinov.AsteroidsClone.Unit.Weapon
{
    public class LaserLogic : WeaponLogic
    {
        public LaserLogic(GameObject gameObject)
            :base(gameObject) {}

        public void Attack(ref int ammo)
        {
            if (ammo > 0)
            {
                var objectPool = gameObject.GetComponent<ObjectPool>();
                var projectile = objectPool.GetAvailableObject();

                projectile.transform.position = gameObject.transform.position;
                projectile.transform.up = gameObject.transform.up;
                projectile.SetActive(true);

                ammo -= 1;
            }
        }

        public void RestoreAmmo(ref int ammo, ref float restoreAmmoTime, float initialRestoreAmmoTime)
        {
            if (ammo < 5)
            {
                restoreAmmoTime -= Time.deltaTime;
                if (restoreAmmoTime < 0)
                {
                    ammo++;
                    restoreAmmoTime = initialRestoreAmmoTime;
                }
            }
        } 
    }
}
