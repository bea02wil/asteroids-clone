
namespace DmitryKonstantinov.AsteroidsClone.Unit.Weapon
{
    public class Laser : UnitWeapon
    {
        public int ammo;
        public float restoreAmmoTime;

        private LaserLogic laserLogic;
        private float initialRestoreAmmoTime;

        public int DefaultAmmoCount { private set; get; }

        private void Awake()
        {
            laserLogic = new LaserLogic(gameObject);

            initialRestoreAmmoTime = restoreAmmoTime;
            DefaultAmmoCount = ammo;
        }

        private void Update()
        {
            laserLogic.RestoreAmmo(ref ammo, ref restoreAmmoTime, initialRestoreAmmoTime);
        }

        public override void Attack()
        {
            laserLogic.Attack(ref ammo);
        }
    }
}