using UnityEngine;

namespace DmitryKonstantinov.AsteroidsClone.Unit.Weapon
{
    public class GunProjectile : Projectile
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag != "Portal")
            {
                projectileLogic.DestroyImmediately(gameObject);
            }
        }
    }
}
