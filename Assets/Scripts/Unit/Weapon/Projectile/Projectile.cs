using UnityEngine;

namespace DmitryKonstantinov.AsteroidsClone.Unit.Weapon
{
    public abstract class Projectile : MonoBehaviour
    {
        [SerializeField] private Projectile_SO projectileData;

        protected ProjectileTransformLogic projectileTransformLogic;
        protected ProjectileLogic projectileLogic;
        protected Rigidbody2D rigidBody;

        protected virtual void OnEnable()
        {
            rigidBody = GetComponent<Rigidbody2D>();
            projectileTransformLogic = new ProjectileTransformLogic(transform);
            projectileLogic = new ProjectileLogic();

            StartCoroutine(projectileLogic.Destroy(gameObject, projectileData.destroyTime));
        }

        protected virtual void FixedUpdate()
        {
            rigidBody.velocity = projectileTransformLogic.Move(projectileData.moveSpeed);
        }
    }
}
