using System.Collections;
using UnityEngine;

namespace DmitryKonstantinov.AsteroidsClone.Unit.Weapon
{
    public class ProjectileLogic
    {
        public IEnumerator Destroy(GameObject gameObject, float destroyTime)
        {
            yield return new WaitForSecondsRealtime(destroyTime);

            Disable(gameObject);
        }

        public void DestroyImmediately(GameObject gameObject)
        {
            Disable(gameObject);
        }

        private void Disable(GameObject gameObject)
        {
            gameObject.SetActive(false);
        }
    }
}

