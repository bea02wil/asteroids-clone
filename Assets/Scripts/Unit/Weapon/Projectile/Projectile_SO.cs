using UnityEngine;

namespace DmitryKonstantinov.AsteroidsClone.Unit.Weapon
{
    [CreateAssetMenu(fileName = "Projectile Data", menuName = "Data/ProjectileData")]
    public class Projectile_SO : ScriptableObject
    {
        public float moveSpeed;
        public float destroyTime;
    }
}
