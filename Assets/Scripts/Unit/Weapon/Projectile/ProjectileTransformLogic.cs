using DmitryKonstantinov.AsteroidsClone.Utils;
using UnityEngine;

namespace DmitryKonstantinov.AsteroidsClone.Unit.Weapon
{
    public class ProjectileTransformLogic : ObjectTransform
    {
        public ProjectileTransformLogic(Transform objectTransform)
            : base(objectTransform) { }
    }
}

