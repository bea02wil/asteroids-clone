
namespace DmitryKonstantinov.AsteroidsClone.Unit.Weapon
{
    public interface IAttackable
    {
        public void Attack(UnitWeapon weapon);
    }
}
