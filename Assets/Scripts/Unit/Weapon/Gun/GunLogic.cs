using DmitryKonstantinov.AsteroidsClone.Utils;
using UnityEngine;

namespace DmitryKonstantinov.AsteroidsClone.Unit.Weapon
{
    public class GunLogic : WeaponLogic
    {
        public GunLogic(GameObject gameObject) 
            :base(gameObject) {}

        public void Attack()
        {
            var objectPool = gameObject.GetComponent<ObjectPool>();
            var projectile = objectPool.GetAvailableObject();

            projectile.transform.position = gameObject.transform.position;
            projectile.transform.up = gameObject.transform.up;
            projectile.SetActive(true);
        }
    }
}
