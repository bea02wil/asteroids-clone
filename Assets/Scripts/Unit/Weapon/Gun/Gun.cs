
namespace DmitryKonstantinov.AsteroidsClone.Unit.Weapon
{
    public class Gun : UnitWeapon
    {
        private GunLogic gunLogic;

        private void Awake()
        {
            gunLogic = new GunLogic(gameObject);
        }

        public override void Attack()
        {
            gunLogic.Attack();
        }
    }
}