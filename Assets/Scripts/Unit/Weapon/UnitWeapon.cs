using UnityEngine;

namespace DmitryKonstantinov.AsteroidsClone.Unit.Weapon
{
    public abstract class UnitWeapon : MonoBehaviour
    {      
        public abstract void Attack();
    }
}
