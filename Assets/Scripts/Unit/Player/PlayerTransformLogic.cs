using DmitryKonstantinov.AsteroidsClone.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace DmitryKonstantinov.AsteroidsClone.Unit
{
    public class PlayerTransformLogic : ObjectTransform
    {
        public PlayerTransformLogic(Transform objectTransform)
            : base(objectTransform) { }

        public void GoForward(Rigidbody2D rigidBody, UnitData_SO unitData)
        {
            if (Keyboard.current.wKey.isPressed)
            {
                rigidBody.velocity = Move(ref unitData.moveSpeed,
                    unitData.acceleration, unitData.maxSpeed);
            }
            else
            {
                //�������
                KeepCoasting(ref unitData.moveSpeed, rigidBody.velocity.y);
            }
        }

        private void KeepCoasting(ref float moveSpeed, float yVelocity)
        {
            moveSpeed = Mathf.Abs(yVelocity);
        }

        public void Rotate(float rotationAngle)
        {
            if (Keyboard.current.dKey.isPressed)
            {
                TurnSide(Vector3.back, rotationAngle);
            }
            else if (Keyboard.current.aKey.isPressed)
            {
                TurnSide(Vector3.forward, rotationAngle);
            }     
        }

        private void TurnSide(Vector3 direction, float rotationAngle)
        {
            objectTransform.Rotate(direction, rotationAngle * Time.deltaTime);
        }

        public void PlaceInCentreOfScreen()
        {
            var centreOfScreenPoint = Vector2.zero;

            objectTransform.position = centreOfScreenPoint;
        }
    }
}
