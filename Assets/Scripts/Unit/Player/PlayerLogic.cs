using DmitryKonstantinov.AsteroidsClone.Unit.Weapon;
using UnityEngine;
using UnityEngine.InputSystem;

namespace DmitryKonstantinov.AsteroidsClone.Unit
{
    public class PlayerLogic : UnitLogic, IAttackable
    {
        private Player player;

        public PlayerLogic(Player player)
        {
            this.player = player;
        }

        public void Attack(UnitWeapon weapon)
        {
            if (Keyboard.current.spaceKey.wasPressedThisFrame)
            {
                weapon.Attack();
            }  
        }

        public void SwapWeapon(UnitData_SO unitData)
        {
            if (Keyboard.current.digit1Key.wasPressedThisFrame)
            {
                unitData.unitWeapon = player.gameObject.GetComponentInChildren<Gun>();
            }
            else if (Keyboard.current.digit2Key.wasPressedThisFrame)
            {
                unitData.unitWeapon = player.gameObject.GetComponentInChildren<Laser>();
            }
        }

        public override void TriggerEnter2DHandler(GameObject gameObject, Collider2D other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("Enemy"))
            {
                player.IsDead = true;
                gameObject.SetActive(false);
            }
        }
    }
}