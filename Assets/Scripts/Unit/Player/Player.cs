using DmitryKonstantinov.AsteroidsClone.Unit.Weapon;
using UnityEngine;

namespace DmitryKonstantinov.AsteroidsClone.Unit
{
    public class Player : Unit
    {
        private PlayerTransformLogic playerTransformLogic;
        private PlayerLogic playerLogic;

        public bool IsDead { get; set; }

        protected override void Awake()
        {
            base.Awake();

            UnitData.unitWeapon = GetComponentInChildren<Gun>();
            UnitData.moveSpeed = 0;

            playerTransformLogic = new PlayerTransformLogic(transform);
            playerLogic = new PlayerLogic(this);

            playerTransformLogic.PlaceInCentreOfScreen();
        }

        private void Update()
        {
            playerLogic.SwapWeapon(UnitData);

            playerLogic.Attack(UnitData.unitWeapon);

            playerTransformLogic.Rotate(UnitData.angleToRotate);
        }

        private void FixedUpdate()
        {
            playerTransformLogic.GoForward(rigidBody, UnitData);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            playerLogic.TriggerEnter2DHandler(gameObject, other);
        }
    }
}

