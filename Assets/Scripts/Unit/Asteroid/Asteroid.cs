using UnityEngine;

namespace DmitryKonstantinov.AsteroidsClone.Unit
{
    public class Asteroid : Unit
    {
        //����� �������, ��� scriptable ���������� (UnitData.moveSpeed � ������ ������)
        //������� �������� ��� ���� ��������� � ����� ����������� ���� ��������� ����� ��������
        //, ���� �� ��������, ������� ���� ��-��, � scriptableObject's �� ���� ������� � ��������
        //��� ���� �������, ��� ���� ��������� SO �� ����������.
        public float CurrentSpeed { get; set; }

        private AsteroidTransformLogic asteroidTransformLogic;
        private AsteroidLogic asteroidLogic;

        protected override void Awake()
        {
            base.Awake();

            asteroidTransformLogic = new AsteroidTransformLogic(transform);
            asteroidLogic = new AsteroidLogic();
            CurrentSpeed = UnitData.moveSpeed;
        }

        private void OnDisable()
        {
            asteroidLogic.destroy?.Invoke(UnitData.points);
        }

        private void FixedUpdate()
        {
            rigidBody.velocity = asteroidTransformLogic.Move(CurrentSpeed);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            asteroidLogic.TriggerEnter2DHandler(gameObject, other);
        }
    }
}
