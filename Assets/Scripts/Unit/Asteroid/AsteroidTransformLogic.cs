using UnityEngine;
using DmitryKonstantinov.AsteroidsClone.Utils;

namespace DmitryKonstantinov.AsteroidsClone.Unit
{
    public class AsteroidTransformLogic : ObjectTransform
    {
        public AsteroidTransformLogic(Transform objectTransform)
            : base(objectTransform) { } 
    }
}
