using System.Collections.Generic;
using UnityEngine;
using DmitryKonstantinov.AsteroidsClone.Utils;

namespace DmitryKonstantinov.AsteroidsClone.Unit
{
    public class AsteroidLogic : UnitLogic
    {
        private int piecesCount = 2;
        private UnitSpawner unitSpawner;

        public AsteroidLogic() : base()
        {         
            unitSpawner = Object.FindObjectOfType<UnitSpawner>();
        }

        public override void TriggerEnter2DHandler(GameObject gameObject, Collider2D other)
        {
            if (other.tag == "GunProjectile")
            {
                switch (gameObject.tag)
                {
                    case "AsteroidHuge":
                    case "AsteroidMed":
                        var angleToRotate = 45f;
                        var extraSpeed = 0.5f;

                        var asteroidPieces = FallToPieces(gameObject, piecesCount);

                        SetPiecesDirection(gameObject, asteroidPieces, angleToRotate);

                        IncreasePiecesMoveSpeed(asteroidPieces, extraSpeed);

                        break;
                    case "AsteroidSmall":
                        break;
                    default:
                        break;
                }
            }
            if (other.tag == "GunProjectile" || other.tag == "LaserProjectile")
            {
                Destroy(gameObject);
            }         
        }

        private List<GameObject> FallToPieces(GameObject asteroidGameObject, int piecesCount)
        {
            var tag = asteroidGameObject.tag == "AsteroidHuge" ? "AsteroidMed" : "AsteroidSmall";

            var asteroids = unitSpawner.unitSpawnerLogic.SpawnUnitsAsPieces(tag, piecesCount);

            return asteroids;
        }

        private void SetPiecesDirection(GameObject baseAsteroid, List<GameObject> asteroids, float angle)
        {
            foreach (var asteroid in asteroids)
            {
                asteroid.transform.position = baseAsteroid.transform.position;

                asteroid.transform.rotation =
                    baseAsteroid.transform.rotation * Quaternion.Euler(0, 0, angle);

                angle *= -1;
            }
        } 

        private void IncreasePiecesMoveSpeed(List<GameObject> asteroids, float extraSpeed)
        {
            foreach (var asteroid in asteroids)
            {
                var asteroidComponent = asteroid.GetComponent<Asteroid>();

                asteroidComponent.CurrentSpeed += extraSpeed;
            }
        }
    }
}
