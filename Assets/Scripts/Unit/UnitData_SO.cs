using DmitryKonstantinov.AsteroidsClone.Unit.Weapon;
using UnityEngine;

namespace DmitryKonstantinov.AsteroidsClone.Unit
{
    [CreateAssetMenu(fileName = "Unit Data", menuName = "Data/UnitData")]
    public class UnitData_SO : ScriptableObject
    {
        [HideInInspector] public UnitWeapon unitWeapon;

        public Sprite explosion;

        public float moveSpeed;
        public float acceleration;
        public float maxSpeed;
        public float angleToRotate;

        public int points;
    }
}

