using DmitryKonstantinov.AsteroidsClone.Utils;
using System;
using UnityEngine;

namespace DmitryKonstantinov.AsteroidsClone.Unit
{
    public class UnitLogic
    {
        protected ScoreCounter scoreCounter;

        public Action<int> destroy;

        protected UnitLogic()
        {
            scoreCounter = UnityEngine.Object.FindObjectOfType<ScoreCounter>();
        }

        protected void Destroy(GameObject gameObject)
        {
            var spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
            var unit = gameObject.GetComponent<Unit>();

            var destroyDelay = 0.1f;

            destroy = scoreCounter.AddPlayerScore;

            spriteRenderer.sprite = unit.UnitData.explosion;
            UnityEngine.Object.Destroy(gameObject, destroyDelay);
        }

        public virtual void TriggerEnter2DHandler(GameObject gameObject, Collider2D other) { }
    }
}
