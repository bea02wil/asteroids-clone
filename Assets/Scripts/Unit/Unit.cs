using UnityEngine;

namespace DmitryKonstantinov.AsteroidsClone.Unit
{
    public abstract class Unit : MonoBehaviour
    {
        public UnitData_SO UnitData;
        protected Rigidbody2D rigidBody;

        protected virtual void Awake()
        {
            rigidBody = GetComponent<Rigidbody2D>();
        }
    }
}
