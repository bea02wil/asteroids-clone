using UnityEngine;

namespace DmitryKonstantinov.AsteroidsClone.Unit
{
    public class UfoLogic : UnitLogic
    {
        public UfoLogic() : base() { }

        public override void TriggerEnter2DHandler(GameObject gameObject, Collider2D other)
        {
            if (other.tag == "GunProjectile" || other.tag == "LaserProjectile")
            {
                Destroy(gameObject);
            }               
        }
    }
}
