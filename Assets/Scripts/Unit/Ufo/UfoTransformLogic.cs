using UnityEngine;
using DmitryKonstantinov.AsteroidsClone.Utils;

namespace DmitryKonstantinov.AsteroidsClone.Unit
{
    public class UfoTransformLogic : ObjectTransform
    {
        public UfoTransformLogic(Transform objectTransform)
            : base(objectTransform) { }

        public void FollowPlayer(Player player, float moveSpeed)
        {
            if (player != null)
            {
                objectTransform.transform.position = Vector2.MoveTowards(
                    objectTransform.position,
                    player.transform.position,
                    moveSpeed * Time.deltaTime
                );
            }          
        }

        public void SetDefaultRotation()
        {
            objectTransform.rotation = Quaternion.identity;
        }
    }
}
