using UnityEngine;

namespace DmitryKonstantinov.AsteroidsClone.Unit
{
    public class Ufo : Unit
    {
        private Player player;
        private UfoTransformLogic ufoTransformLogic;
        private UfoLogic ufoLogic;

        private void OnEnable()
        {
            player = FindObjectOfType<Player>();

            ufoLogic = new UfoLogic();
            ufoTransformLogic = new UfoTransformLogic(transform);
            ufoTransformLogic.SetDefaultRotation();
        }

        private void OnDisable()
        {
            ufoLogic.destroy?.Invoke(UnitData.points);
        }

        private void Update()
        {
            ufoTransformLogic.FollowPlayer(player, UnitData.moveSpeed);   
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            ufoLogic.TriggerEnter2DHandler(gameObject, other);
        }
    }
}
