using UnityEngine;

namespace DmitryKonstantinov.AsteroidsClone.Portals
{
    public enum ScreenSides 
    { 
        Top, 
        Right, 
        Bottom, 
        Left 
    }

    public class PortalHandler : MonoBehaviour
    {   
        [SerializeField] private ScreenSides screenSide;
        private PortalHandlerLogic portalHandlerLogic;

        private void Awake()
        {
            portalHandlerLogic = new PortalHandlerLogic();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            portalHandlerLogic.TeleportToSide(other, screenSide);
        }
    }
}
