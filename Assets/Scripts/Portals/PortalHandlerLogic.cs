using UnityEngine;

namespace DmitryKonstantinov.AsteroidsClone.Portals
{
    public class PortalHandlerLogic
    {
        private void TeleportToPoint(Collider2D obj, int coordinate, char axis)
        {
            obj.transform.position = axis == 'Y'
               ? new Vector2(obj.transform.position.x, coordinate)
               : new Vector2(coordinate, obj.transform.position.y);
        }

        public void TeleportToSide(Collider2D obj, ScreenSides screenSide)
        {
            switch (screenSide)
            {
                case ScreenSides.Top:
                    TeleportToPoint(obj, -5, 'Y');
                    break;
                case ScreenSides.Right:
                    TeleportToPoint(obj, -10, 'X');
                    break;
                case ScreenSides.Bottom:
                    TeleportToPoint(obj, 5, 'Y');
                    break;
                case ScreenSides.Left:
                    TeleportToPoint(obj, 10, 'X');
                    break;
                default:
                    break;
            }
        }
    }
}
