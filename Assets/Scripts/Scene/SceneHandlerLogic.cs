using UnityEngine.SceneManagement;

namespace DmitryKonstantinov.AsteroidsClone.Scene
{
    public class SceneHandlerLogic
    {
        public void ReloadScene()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
