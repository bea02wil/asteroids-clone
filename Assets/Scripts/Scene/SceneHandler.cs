using UnityEngine;

namespace DmitryKonstantinov.AsteroidsClone.Scene
{
    public class SceneHandler : MonoBehaviour
    {
        private SceneHandlerLogic sceneHandlerLogic;

        private void Awake()
        {
            sceneHandlerLogic = new SceneHandlerLogic();
        }

        public void ReloadScene()
        {
            sceneHandlerLogic.ReloadScene();
        }
    }

}
