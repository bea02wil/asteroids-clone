using System.Collections.Generic;
using UnityEngine;

namespace DmitryKonstantinov.AsteroidsClone.Utils
{
    public class ObjectPoolLogic
    {
        private List<GameObject> pool;
        private GameObject prefabObject;

        private int poolDepth;
        private bool canGrow;

        public ObjectPoolLogic(List<GameObject> pool,
            GameObject prefabObject, int poolDepth, bool canGrow)
        {
            this.pool = pool;
            this.prefabObject = prefabObject;

            this.poolDepth = poolDepth;
            this.canGrow = canGrow;
        }

        public void InitialPoolFill()
        {
            if (poolDepth > 0)
            {
                for (int i = 0; i < poolDepth; i++)
                {
                    var pooledObject = Object.Instantiate(prefabObject);
                    pooledObject.SetActive(false);
                    pool.Add(pooledObject);
                }
            }
        }

        public void ClearPool()
        {
            if (poolDepth == 0) pool.Clear();
        }

        public GameObject GetNotActivePoolObject()
        {
            for (int i = 0; i < pool.Count; i++)
            {
                if (pool[i].activeInHierarchy == false)
                {
                    return pool[i];
                }
            }

            return null;
        }

        public GameObject Instantiate(GameObject changedPrefabObject = null)
        {
            if (canGrow)
            {
                var instantiatedObject = prefabObject == null ? changedPrefabObject : prefabObject;
                var pooledObject = Object.Instantiate(instantiatedObject);

                pooledObject.SetActive(false);

                pool.Add(pooledObject);

                return pooledObject;
            }
            else
            {
                return null;
            }
        }
    }
}
