using System.Collections.Generic;
using UnityEngine;

namespace DmitryKonstantinov.AsteroidsClone.Utils
{
    public class ObjectPool : MonoBehaviour
    {
        [SerializeField] private int poolDepth;
        [SerializeField] private bool canGrow = true;

        public GameObject prefabObject;

        private readonly List<GameObject> pool = new List<GameObject>();

        private ObjectPoolLogic objectPoolLogic;

        private void Awake()
        {
            objectPoolLogic = new ObjectPoolLogic(pool, prefabObject, poolDepth, canGrow);
            objectPoolLogic.InitialPoolFill();
        }

        public GameObject GetAvailableObject()
        {            
            objectPoolLogic.ClearPool();

            var notActiveObject = objectPoolLogic.GetNotActivePoolObject();
            if (notActiveObject != null) return notActiveObject;

            var instantiatedObject = objectPoolLogic.Instantiate(prefabObject);
 
            return instantiatedObject;
        }
    }
}
