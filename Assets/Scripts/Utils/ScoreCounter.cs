using UnityEngine;

namespace DmitryKonstantinov.AsteroidsClone.Utils
{
    public class ScoreCounter : MonoBehaviour
    {
        public int playerScore;

        public void AddPlayerScore(int score)
        {
            playerScore += score;
        }
    }
}
