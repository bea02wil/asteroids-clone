using UnityEngine;

namespace DmitryKonstantinov.AsteroidsClone.Utils
{
    public class SingletonHandler<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T instance;

        public static T Instance
        {
            get => instance;
            set
            {
                if (instance == null)
                {
                    instance = value;
                }

            }
        }

        protected virtual void Awake()
        {
            Instance = this as T;
        }
    }
}

