using UnityEngine;

namespace DmitryKonstantinov.AsteroidsClone.Utils
{
    public abstract class ObjectTransform
    {
        protected Transform objectTransform;

        public ObjectTransform(Transform objectTransform)
        {
            this.objectTransform = objectTransform;
        }

        public Vector2 Move(float moveSpeed)
        {
            var velocity = ComputeVelocity(moveSpeed);
            return velocity;
        }

        public Vector2 Move(ref float moveSpeed, float acceleration, float maxSpeed)
        {
            moveSpeed = Mathf.Min(moveSpeed + acceleration, maxSpeed);
            var velocity = ComputeVelocity(moveSpeed);
            return velocity;
        }

        private Vector2 ComputeVelocity(float moveSpeed)
        {
            var velocity = -objectTransform.up * moveSpeed;

            return velocity;
        }
    }
}

