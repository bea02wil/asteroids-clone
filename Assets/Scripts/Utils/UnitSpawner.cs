using UnityEngine;

namespace DmitryKonstantinov.AsteroidsClone.Utils
{
    public class UnitSpawner : MonoBehaviour
    {
        [SerializeField] private GameObject[] units;
        [SerializeField] private Transform[] posObjects;
        
        [Header("Range settings.  Max - exclusive")]
        [SerializeField] private int minUnitCountPerOneSpawn;
        [SerializeField] private int maxUnitCountPerOneSpawn;

        [SerializeField] private int minSpawnTime;
        [SerializeField] private int maxSpawnTime;

        public UnitSpawnerLogic unitSpawnerLogic;
        private ObjectPool objectPool;

        private void Awake()
        {
            objectPool = GetComponent<ObjectPool>();

            unitSpawnerLogic = new UnitSpawnerLogic(objectPool, units);
            StartCoroutine(unitSpawnerLogic.SpawnUnits(
                posObjects,
                minUnitCountPerOneSpawn,
                maxUnitCountPerOneSpawn,
                minSpawnTime,
                maxSpawnTime
               ));
        }
    }
}
