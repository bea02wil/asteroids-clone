using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DmitryKonstantinov.AsteroidsClone.Utils
{
    public class UnitSpawnerLogic
    {
        private ObjectPool objectPool;
        private GameObject[] units;

        private int spawnTime;
        private int unitCountPerOneSpawn;

        public UnitSpawnerLogic() { }

        public UnitSpawnerLogic(ObjectPool objectPool, GameObject[] units)
        {
            this.objectPool = objectPool;
            this.units = units;
        }

        public IEnumerator SpawnUnits(
            Transform[] posObjects, int minUnitCount,
            int maxUnitCount, int minSpawntime, int maxSpawnTime)
        {
            while (true)
            {
                unitCountPerOneSpawn = UnityEngine.Random.Range(minUnitCount, maxUnitCount);

                for (int i = 0; i < unitCountPerOneSpawn; i++)
                {
                    var randomPositionObjectIndex = UnityEngine.Random.Range(0, posObjects.Length);
                    var randomUnitIndex = UnityEngine.Random.Range(0, units.Length);

                    objectPool.prefabObject = units[randomUnitIndex];

                    var unit = objectPool.GetAvailableObject();
                    unit.transform.position = posObjects[randomPositionObjectIndex].position;
                    unit.transform.rotation = Quaternion.Euler(new Vector3(0, 0, UnityEngine.Random.Range(-360, 360)));
                    unit.gameObject.SetActive(true);
                }
                spawnTime = UnityEngine.Random.Range(minSpawntime, maxSpawnTime);

                yield return new WaitForSecondsRealtime(spawnTime);
            }
        }

        public List<GameObject> SpawnUnitsAsPieces(string parentTag, int count)
        {
            var unitPieces = new List<GameObject>();

            var parentObject = Array.Find(units, unit => unit.tag == parentTag);
            objectPool.prefabObject = parentObject;

            for (int i = 0; i < count; i++)
            {
                var piece = objectPool.GetAvailableObject();
                piece.SetActive(true);

                unitPieces.Add(piece);
            }

            return unitPieces;
        }
    }
}
